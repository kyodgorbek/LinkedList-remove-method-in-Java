# LinkedList-remove-method-in-Java




import java.util.LinkedList;

public class RemoveFirstElementsLinkedListExample {

 public static void main(String args[] ){
  
  //create LinkedList object
   LinkedList IList = new LinkedList();
  //add element to LinkedList
  IList.add("1");
  IList.add("2");
  IList.add("3");
  IList.add("4");
  IList.add("5");
  
  System.out.println("LinkedList contains: " + IList);
  
  Object object = IList.removeFirst();
  System.out.println(object+ "has been removed from the first index of LinkedList");
  System.out.println("LinkedList now contains: " + IList);
 }
}  
  
